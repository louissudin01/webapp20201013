﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp20201017.Models
{
    public class Testing
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public int Match { get; set; }
        public static Testing FromCsv( string csvLine)
        {
            string[] values = csvLine.Split(';');
            Testing objTesting = new Testing();
            objTesting.Id = values[0];
            objTesting.Content = values[1];
            return objTesting;
        }
    }

}