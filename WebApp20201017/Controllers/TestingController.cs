﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp20201017.Models;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using CsvHelper;
using WebApp20201017.ViewModels;

namespace WebApp20201017.Controllers
{
    public class TestingController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        internal static readonly char[] chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890[]".ToCharArray();

        private static string GetUniqueKey()
        {
            byte[] data = new byte[1024];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(1024);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        private static Random random = new Random();
        private static string GetUniqueKey2()
        {
            return new string(Enumerable.Repeat(chars, 1024)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static int matchData(string FullContent,string searchContent)
        {
            if (FullContent == null || searchContent == null)
            {
                return 0;
            }
            var fullLength = FullContent.Length;
            var searchLength = searchContent.Length;
            var totalCount = 0;
            for (var i = 0; i < fullLength; i++)
            {
                var j = 0;
                for (j = 0; j < searchLength; j++)
                {
                    if (i + j >= fullLength)
                    {
                        break;
                    }
                    if (FullContent[i + j] != searchContent[j])
                    {
                        break;
                    }
                }
                if (j == searchLength)
                {
                    totalCount += 1;
                }
            }
            return totalCount;
        }


        public ActionResult InsertData()
        {

            List<Testing> tmpTest = new List<Testing> { };
            for (int i = 0; i < 100000; i++)
            {
                tmpTest.Add(new Testing { Id = Guid.NewGuid().ToString(), Content = GetUniqueKey() });

            }

            string csvPath = Server.MapPath("~/Files/testing.csv");

            using (var mem = new MemoryStream())
            using (var writer = new StreamWriter(mem))
            using (var csvWriter = new CsvWriter(writer, System.Globalization.CultureInfo.CurrentCulture))
            {

                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.HasHeaderRecord = true;
                csvWriter.Configuration.AutoMap<Testing>().Map(m=>m.Match).Ignore();
 
                csvWriter.WriteHeader<Testing>();
                csvWriter.NextRecord();
                csvWriter.WriteRecords(tmpTest);

                writer.Flush();
                var result = Encoding.UTF8.GetString(mem.ToArray());
                Console.WriteLine(result);
                System.IO.File.WriteAllText(csvPath, result);
            }

            

            return RedirectToAction("Index");
        }

        public ActionResult SearchData(testingsViewModel tmpModel)
        {

            string csvPath = Server.MapPath("~/Files/testing.csv");
            string ReadCSV = System.IO.File.ReadAllText(csvPath);
            List<Testing> tmptestings = System.IO.File.ReadAllLines(csvPath).Skip(1)
                                    .Select(v => Testing.FromCsv(v)).ToList();
            List<Testing> testings = new List<Testing>();
            
            if (tmpModel.searchString == null)
            {
                var viewModel2 = new testingsViewModel
                {
                    TestingList = tmptestings
                };
                return View("Index", viewModel2);
            }
            for (int i=0; i< tmptestings.Count;i++)
            {
                int tmpMatch = matchData(tmptestings[i].Content, tmpModel.searchString);
                if (tmpMatch > 0) 
                {
                    tmptestings[i].Match = tmpMatch;
                    testings.Add(tmptestings[i]);
                }

            }
            
            var viewModel = new testingsViewModel
            {
                TestingList = testings
            };
                 
            return View("Index",viewModel);
        }
    }
}