﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp20201017.Models;

namespace WebApp20201017.ViewModels
{
    public class testingsViewModel
    {
        public string searchString { get; set; }
        public int match { get; set; }
        public List<Testing> TestingList { get; set; }
        
    }
}